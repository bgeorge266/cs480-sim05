#ifndef READ_METADATA_H
#define READ_METADATA_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "file_helpers.h"


struct md_command
{
    char component;
    char descriptor[11];
    int cycles;
    int memory;
    float time_to_run;
    struct md_command *next;
};

int check_component(char component);

int check_descriptor(char* descriptor);

int check_descriptor_matches_component(char component, char* descriptor);

struct error_data prepare_metadata(char* metadata_filepath, int* commands_count);

void set_time_to_run(struct md_command *current_command, int IO_cycle_time, int CPU_cycle_time);

struct md_command link_commands(struct md_command* commands, int commands_count);

struct error_data read_metadata(char* metadata_filepath, int commands_count, int IO_cycle_time, int CPU_cycle_time, struct md_command* first_command);


#endif