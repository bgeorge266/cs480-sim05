#ifndef READ_CONFIG_H
#define READ_CONFIG_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "file_helpers.h"


struct config_data
{
    int version;
    char mdf_filepath[60];
    char CPU_scheduling[10];
    int quantum_time;
    int memory_available;
    int CPU_cycle_time;
    int IO_cycle_time;
    char log_to[8];
    char lgf_filepath[60];
};


int check_value_in_range(int value, int min, int max);

int check_scheduling(char* scheduling);

int check_log_to(char* log_to);

struct error_data read_config(char* config_fp, struct config_data *config);


#endif