CC = gcc
DEBUG = -g
CFLAGS = -Wall -std=c99 -c $(DEBUG)
LFLAGS = -Wall -std=c99 $(DEBUG)
PTHREAD = -lpthread

sim05 : sim05.o file_helpers.o read_config.o read_metadata.o manage_pcbs.o run_simulation.o stopwatch.o
	$(CC) $(LFLAGS) sim05.o file_helpers.o read_config.o read_metadata.o manage_pcbs.o run_simulation.o stopwatch.o $(PTHREAD) -o sim05 $(DEBUG) -O0

sim05.o : sim05.c sim05.h file_helpers.h read_config.h read_metadata.h manage_pcbs.h run_simulation.h stopwatch.h
	$(CC) $(CFLAGS) sim05.c

file_helpers.o : file_helpers.c file_helpers.h
	$(CC) $(CFLAGS) file_helpers.c

read_config.o : read_config.c read_config.h
	$(CC) $(CFLAGS) read_config.c

read_metadata.o : read_metadata.c read_metadata.h
	$(CC) $(CFLAGS) read_metadata.c

manage_pcbs.o : manage_pcbs.c manage_pcbs.h
	$(CC) $(CFLAGS) manage_pcbs.c

run_simulation.o : run_simulation.c run_simulation.h
	$(CC) $(CFLAGS) run_simulation.c

stopwatch.o : stopwatch.c stopwatch.h
	$(CC) $(CFLAGS) stopwatch.c

clean:
	\rm *.o sim05