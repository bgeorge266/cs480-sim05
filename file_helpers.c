#ifndef FILEHELPERS_C
#define FILEHELPERS_C

#include "file_helpers.h"

int check_file_ext(char* filename, char* expected_ext)
{
    int filename_length = strlen(filename);
    char filename_ext[5] = {'\0'};
    int char_count = 0;
    int filename_index = filename_length - 4;
    
    for (; filename_index < filename_length; filename_index++)
    {
        filename_ext[char_count] = filename[filename_index];
        char_count++;
    }

    if (strcmp(filename_ext, expected_ext) != 0)
    {
        return 1;
    }

    return 0;
}


int check_fh_is_valid(FILE* file_handle)
{
    if (!file_handle)
    {
        return 1;
    }

    return 0;
}


int check_line(char* expected_line, char* received_line)
{
    if (strcmp(expected_line, received_line) != 0)
    {
        return 1;
    }

    return 0;
}


void skip_whitespace(FILE* file_handle)
{
    char current_char = fgetc(file_handle);

    while (isspace(current_char))
    {
        current_char = fgetc(file_handle);
    }

    ungetc(current_char, file_handle);

    return;
}

void reset_string(char* array)
{
    int index = 0;
    while (array[index] != '\0')
    {
        array[index++] = '\0';
    }
}

#endif