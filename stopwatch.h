#ifndef STOPWATCH_H
#define STOPWATCH_H


#include <string.h>
#include <sys/time.h>
#include <time.h>

#include "run_simulation.h"


struct wait_P_data_struct
{
	float time_to_wait_us;
	struct PCB_queue* blocked;
	struct PCB_queue* interrupts;
	struct PCB_struct* process;
};


void timeToString( int secTime, int uSecTime, char *timeStr );

char *get_time(char* operation, long* current_time, struct timeval *before, struct timeval *after);

void wait(int time_to_run);

void * threaded_wait_N(void *time_to_wait_us);

void * threaded_wait_P(void *wait_P_data_struct);


#endif