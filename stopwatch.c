#ifndef STOPWATCH_C
#define STOPWATCH_C


#include "stopwatch.h"


// this function taken from Michael Leverington's SimpleTimer.c
/* This is a bit of a drawn-out function, but it is written
   to force the time result to always be in the form x.xxxxxxx
   when printed as a string; this will not always be the case
   when the time is presented as a floating point number
*/
void timeToString(int secTime, int uSecTime, char *timeStr)
{
	int low, high, index = 0;
	char temp;

	while (uSecTime > 0)
	{
		timeStr[index] = (char)(uSecTime % 10 + '0');
		uSecTime /= 10;

		index++;
	}

	while (index < 6)
	{
		timeStr[ index ] = '0';

		index++;
	}

	timeStr[ index ] = '.';

	index++;

	if (secTime == 0)
	{
		timeStr[index] = '0';

		index++;
	}

	while(secTime > 0)
	{
		timeStr[index] = (char)(secTime % 10 + '0');

		secTime /= 10;

		index++;
	}

	timeStr[index] = '\0';

	low = 0;
	high = index - 1;

	while (low < high)
	{
		temp = timeStr[low];
		timeStr[low] = timeStr[high];
		timeStr[high] = temp;

		low++; high--;
	}
}


char *get_time(char* operation, long* current_time, struct timeval *before, struct timeval *after)
{
	static char time_string[10];
	int seconds, micro;

	if (strcmp(operation, "start") == 0)
	{
		gettimeofday(&(*before), NULL);

		return("Error: Misuse of get_time() function.");
	}

	else if (strcmp(operation, "stop") == 0)
	{
		gettimeofday (&(*after), NULL);

		(*current_time) = (((((*after).tv_sec * 1000000L) + ((*after).tv_usec))
						  - (((*before).tv_sec * 1000000L) + ((*before).tv_usec))) + (*current_time));

		seconds = (*current_time) / 1000000;
		micro = (*current_time) % 1000000;

		// timeToString Author: Michael Leverington
		timeToString(seconds, micro, time_string);
		return time_string;
	}

	else if (strcmp(operation, "wait") == 0)
	{
		gettimeofday (&(*after), NULL);

		return time_string;
	}

	return("Error: Misuse of get_time() function.");
}


void wait(int time_to_run)
{
	long time_waited = 0;
	struct timeval iteration_start_time, iteration_end_time;

	while (time_waited < time_to_run)
	{
		get_time("start", NULL, &iteration_start_time, NULL);
	
		get_time("wait", &time_waited, &iteration_start_time, &iteration_end_time);

		time_waited = ((((iteration_end_time.tv_sec * 1000000L) + (iteration_end_time.tv_usec)) 
					  - ((iteration_start_time.tv_sec * 1000000L) + (iteration_start_time.tv_usec))) + time_waited);
	}
}


void * threaded_wait_N(void *time_to_wait_us)
{
	float *time = (float *)time_to_wait_us;

	wait(*time);

	return NULL;
}


void * threaded_wait_P(void *wait_data_parameter)
{
	struct wait_P_data_struct* wait_P_data = (struct wait_P_data_struct*)wait_data_parameter;
	float time = wait_P_data->time_to_wait_us;
	struct PCB_struct* process = wait_P_data->process;
	struct PCB_queue* blocked = wait_P_data->blocked;
	struct PCB_queue* interrupts = wait_P_data->interrupts;

	wait(time);

	move_process_to_queue(blocked, interrupts, process);

	return NULL;
}


#endif