#ifndef FILEHELPERS_H
#define FILEHELPERS_H


#include <ctype.h>
#include <stdio.h>
#include <string.h>


struct error_data
{
	int error_message;

	// this attribute will only be used for errors in config data
	int line_number;

	// this attribute will only be used for errors in metadata commands
	int command_number;
};

int check_file_ext(char* filename, char* expected_ext);

int check_fh_is_valid(FILE* file_handle);

int check_line(char* expected_line, char* received_line);

void skip_whitespace(FILE* file_handle);

void reset_string(char* array);

#endif