#ifndef SIM05_C
#define SIM05_C


#include "sim05.h"


int main(int argc, char* argv[])
{
    char config_fp[60];
    struct config_data config;
    int CPU_cycle_time, IO_cycle_time;
    char log_filepath[60], scheduling_code[7];
    struct error_data config_results;
    int config_message;
    int error_line_number;

    char metadata_fp[60];

    int commands_count;
    struct error_data prepare_md_results;
    int prepare_md_message;

    struct md_command first_command;
    struct error_data read_md_results;
    int read_md_message;
    int error_command_number;

    struct error_data run_simulation_results;
    int run_simulation_message;


    fflush(stdout);
    fflush(stderr);

    if (argc != 2)
    {
        fprintf(stderr, "Error: Received an incorrect amount of arguments (must be 1).\n");
        exit(1);
    }


    fprintf(stdout, "Operating System Simulator\n");
    fprintf(stdout, "==========================\n\n");

    strcpy(config_fp, argv[1]);
    fprintf(stdout, "Loading configuration file\n");
    config_results = read_config(config_fp, &config);
    config_message = config_results.error_message;
    error_line_number = config_results.line_number;

    switch(config_message)
    {
        case 1:
            fprintf(stderr, "Error: Invalid configuration filename (received %s, expected filename ending in .cnf).\n", config_fp);
            exit(1);

        case 2:
            fprintf(stderr, "Error: Invalid configuration filename (file does not exist).\n");
            exit(1);

        case 3:
            fprintf(stderr, "Error: Line %d of %s contains bad input.\n", error_line_number, config_fp);
            exit(1);

        case 4:
            fprintf(stderr, "Error: Version/Phase listed in configuration file is invalid (must be between 0 and 10).\n");
            exit(1);

        case 5:
            fprintf(stderr, "Error: CPU Scheduling type provided in configuration file is invalid.\n");
            exit(1);

        case 6:
            fprintf(stderr, "Error: Quantum Time (cycles) provided in configuration file is invalid (must be between 0 and 100).\n");
            exit(1);

        case 7:
            fprintf(stderr, "Error: Memory Available (KB) provided in configuration file is invalid (must be between 0 and 1048576).\n");
            exit(1);

        case 8:
            fprintf(stderr, "Error: Processor Cycle Time (msec) provided in configuration file is invalid (must be between 1 and 1000).\n");
            exit(1);

        case 9:
            fprintf(stderr, "Error: I/O Cycle Time (msec) provided in configuration file is invalid (must be between 1 and 10000).\n");
            exit(1);

        case 10:
            fprintf(stderr, "Error: Log To option provided in configuration file is invalid (must be \"File\", \"Monitor\", or \"Both\").\n");
            exit(1);
    }

    CPU_cycle_time = config.CPU_cycle_time;
    IO_cycle_time = config.IO_cycle_time;
    strcpy(log_filepath, config.lgf_filepath);
    strcpy(scheduling_code, config.CPU_scheduling);


    strcpy(metadata_fp, config.mdf_filepath);

    prepare_md_results = prepare_metadata(metadata_fp, &commands_count);
    prepare_md_message = prepare_md_results.error_message;

    switch(prepare_md_message)
    {
        case 1:
            fprintf(stderr, "Error: Metadata filename provided in configuration file is invalid (received %s, expected file extension .mdf).\n", metadata_fp);
            exit(1);

        case 2:
            fprintf(stderr, "Error: Metadata filename provided in configuration file is invalid (file does not exist).\n");
            exit(1);
    }


    fprintf(stdout, "Loading meta-data file\n");
    read_md_results = read_metadata(metadata_fp, commands_count, IO_cycle_time, CPU_cycle_time, &first_command);
    read_md_message = read_md_results.error_message;
    error_command_number = read_md_results.command_number;

    switch(read_md_message)
    {
        case 1:
            fprintf(stderr, "Error: Component of command %d provided in metadata is invalid.\n", error_command_number);
            exit(1);

        case 2:
            fprintf(stderr, "Error: Descriptor of command %d provided in metadata is invalid.\n", error_command_number);
            exit(1);

        case 3:
            fprintf(stderr, "Error: Descriptor of command %d provided in metadata does not match its component.\n", error_command_number);
            exit(1);
    }

    fprintf(stdout, "==========================\n\n");
    fprintf(stdout, "Begin Simulation\n");


    if ((strcmp(scheduling_code, "FCFS-N") == 0) || (strcmp(scheduling_code, "SJF-N") == 0))
    {
        run_simulation_results = run_simulation_N(*first_command.next, commands_count, &config);
        run_simulation_message = run_simulation_results.error_message;
    }

    else
    {
        run_simulation_results = run_simulation_P(*first_command.next, commands_count, &config);
        run_simulation_message = run_simulation_results.error_message;
    }

    if (run_simulation_message == 1)
    {
        fprintf(stderr, "Error: Log filename provided in configuration file is invalid (received %s, expected file extension .lgf).\n", log_filepath);
        exit(1);
    }

    fprintf(stdout, "Simulator is finished.\n");

    exit(0);
}


#endif