#ifndef MANAGE_PCBS_H
#define MANAGE_PCBS_H


#include "read_metadata.h"


struct PCB_struct
{
    int process_num;
    int total_commands_to_execute, commands_executed;
    float remaining_time_to_run;
    int remaining_cycles_in_action;
    struct md_command* process_commands;
    struct PCB_struct *next_PCB;
    struct PCB_struct *prev_PCB;
};


int check_process_start(struct md_command command);

int count_processes(struct md_command current_command, int commands_count);

void find_process_starters(struct md_command current_command, int commands_count, struct md_command* process_starters);

int count_commands_in_process(struct md_command current_command);

void fill_process_array(struct md_command first_command, int commands_in_process, struct md_command* commands_array);

void set_process_runtime(struct PCB_struct* current_PCB);

struct PCB_struct create_PCBs(int processes_count, struct md_command* process_starters, int commands_count);


#endif