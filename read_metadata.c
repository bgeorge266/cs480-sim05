#ifndef READ_METADATA_C
#define READ_METADATA_C


#include "read_metadata.h"


int check_component(char component)
{
    int component_is_correct = 0;
    char components[6] = {"SAPMIO"};
    int component_type = 0;

    for (; component_type < 6; component_type++)
    {
        if (components[component_type] == component)
        {
            component_is_correct = 1;
        }
    }

    if (component_is_correct == 0)
    {
        return 1;
    }

    return 0;
}


int check_descriptor(char* descriptor)
{
    int descriptor_is_correct = 0;
    char* descriptors[9] = {"start", "end", "allocate", "access", "run", "keyboard", "hard drive", "printer", "monitor"};
    int descriptor_type = 0;

    for (; descriptor_type < 9; descriptor_type++)
    {
        if (strcmp(descriptors[descriptor_type], descriptor) == 0)
        {
            descriptor_is_correct = 1;
        }
    }

    if (descriptor_is_correct == 0)
    {
        return 1;
    }

    return 0;
}


int check_descriptor_matches_component(char component, char* descriptor)
{
    int descriptor_matches_component = 0;
    char* S_descriptors[2] = {"start", "end"};
    char* A_descriptors[2] = {"start", "end"};
    char* P_descriptors[1] = {"run"};
    char* M_descriptors[2] = {"allocate", "access"};
    char* I_descriptors[2] = {"hard drive", "keyboard"};
    char* O_descriptors[3] = {"hard drive", "printer", "monitor"};
    int descriptor_type;

    
    switch (component)
    {
        case 'S':
            for (descriptor_type = 0; descriptor_type < 2; descriptor_type++)
            {
                if (strcmp(S_descriptors[descriptor_type], descriptor) == 0)
                {
                    descriptor_matches_component = 1;
                }
            }
            break;

        case 'A':
            for (descriptor_type = 0; descriptor_type < 2; descriptor_type++)
            {
                if (strcmp(A_descriptors[descriptor_type], descriptor) == 0)
                {
                    descriptor_matches_component = 1;
                }
            }
            break;

        case 'P':
            for (descriptor_type = 0; descriptor_type < 1; descriptor_type++)
            {
                if (strcmp(P_descriptors[descriptor_type], descriptor) == 0)
                {
                    descriptor_matches_component = 1;
                }
            }
            break;

        case 'M':
            for (descriptor_type = 0; descriptor_type < 2; descriptor_type++)
            {
                if (strcmp(M_descriptors[descriptor_type], descriptor) == 0)
                {
                    descriptor_matches_component = 1;
                }
            }
            break;

        case 'I':
            for (descriptor_type = 0; descriptor_type < 2; descriptor_type++)
            {
                if (strcmp(I_descriptors[descriptor_type], descriptor) == 0)
                {
                    descriptor_matches_component = 1;
                }
            }
            break;

        case 'O':
            for (descriptor_type = 0; descriptor_type < 3; descriptor_type++)
            {
                if (strcmp(O_descriptors[descriptor_type], descriptor) == 0)
                {
                    descriptor_matches_component = 1;
                }
            }
            break;
    }

    
    if (descriptor_matches_component == 0)
    {
        return 1;
    }

    return 0;
}


// This function handles preparation of the metadata, without processing any of it.
// Performs error checking on the metadata filename, and counts the amount of
// metadata commands contained in the file (to be used later).
struct error_data prepare_metadata(char* metadata_filepath, int* commands_count)
{
    int md_file_ext_is_valid, md_fh_is_valid;
    FILE* metadata_fh;
    char current_char;

    struct error_data prepare_md_results;
    prepare_md_results.error_message = 0;
    prepare_md_results.line_number = 0;
    prepare_md_results.command_number = 0;

    md_file_ext_is_valid = check_file_ext(metadata_filepath, ".mdf");
    if (md_file_ext_is_valid == 1)
    {
        prepare_md_results.error_message = 1;
        return prepare_md_results;
    }

    metadata_fh = fopen(metadata_filepath, "r");
    md_fh_is_valid = check_fh_is_valid(metadata_fh);
    if (md_fh_is_valid == 1)
    {
        prepare_md_results.error_message = 2;
        return prepare_md_results;
    }

    *commands_count = 1;

    current_char = fgetc(metadata_fh);
    while (current_char != EOF)
    {
        if (current_char == ';')
        {
            *commands_count = *commands_count + 1;
        }
        current_char = fgetc(metadata_fh);
    }

    fclose(metadata_fh);

    return prepare_md_results;
}


void set_time_to_run(struct md_command *current_command, int IO_cycle_time, int CPU_cycle_time)
{
    char component = current_command->component;
    int cycles = current_command->cycles;
    float time_to_run;

    if (component == 'M')
    {
        time_to_run = 0;
    }

    else
    {
        if (component == 'I' || component == 'O')
        {
            time_to_run = (float)(cycles * IO_cycle_time);
        }

        else
        {
            time_to_run = (float)(cycles * CPU_cycle_time);
        }
    }

    current_command->time_to_run = time_to_run;
}


struct md_command link_commands(struct md_command* commands, int commands_count)
{
    int commands_linked = 0;
    for (; commands_linked < commands_count - 1; commands_linked++)
    {
        commands[commands_linked].next = &commands[commands_linked + 1];
    }

    return commands[0];
}


struct error_data read_metadata(char* metadata_filepath, int commands_count, int IO_cycle_time, int CPU_cycle_time, struct md_command* first_command)
{
    FILE* metadata_fh = fopen(metadata_filepath, "r");
    char current_char = fgetc(metadata_fh);

    struct md_command* command_array = malloc(commands_count * sizeof(struct md_command));

    // an array to hold strings of the metadata, which will be parsed and stored in structs
    char unparsed_commands[commands_count][18];

    // these variables are used for parsing metadata commands of variable length
    // into strings within a loop
    char current_command[18] = {'\0'}, current_command_char, command_str[18] = {'\0'};

    // these variables are used for parsing through the metadata strings and converting
    // them into structs with the appropriate attributes
    int command, command_str_index;
    char component;
    // these variables are used for parsing a metadata descriptor of variable length
    char command_descriptor[11] = {'\0'}, current_descriptor_char;
    int descriptor_index;
    // these variables are used for parsing a metadata number of variable length
    char command_number[6] = {'\0'}, current_number_char;
    int number_index;
    // these variables track the above parsed metadata number
    // if component == m -> memory = number, cycles = 0
    // else -> memory = 0, cycles = number
    int command_memory;
    int command_cycles;

    struct error_data read_md_results;
    read_md_results.error_message = 0;
    read_md_results.line_number = 0;
    read_md_results.command_number = 0;

    //struct md_command first_command;


    // skip the first line of the metadata file, since it contains no important data
    while (current_char != '\n')
    {
        current_char = fgetc(metadata_fh);
    }

    // parse each command from the metadata file into an unprocessed string,
    // then store each string into an array defined above
    for (command = 0; command < commands_count; command++)
    {
        reset_string(current_command);
        command_str_index = 0;
        current_command_char = fgetc(metadata_fh);
        while (current_command_char != ';' && current_command_char != '.')
        {
            current_command[command_str_index++] = current_command_char;
            current_command_char = fgetc(metadata_fh);
        }

        skip_whitespace(metadata_fh);

        strcpy(unparsed_commands[command], current_command);
    }

    // go through the array of unprocessed strings and extract the metadata
    // command attributes, then store those attributes in the appropriate
    // struct in the array passed to the function
    for (command = 0; command < commands_count; command++)
    {
        strcpy(command_str, unparsed_commands[command]);
        command_str_index = 0;
        component = command_str[command_str_index++];

        int component_is_valid = check_component(component);
        if (component_is_valid == 1)
        {
            read_md_results.error_message = 1;
            read_md_results.command_number = command;
            fclose(metadata_fh);
            return read_md_results;
        }

        // extract the metadata command descriptor (sans parentheses)
        reset_string(command_descriptor);
        current_descriptor_char = command_str[++command_str_index];
        descriptor_index = 0;
        while (current_descriptor_char != ')')
        {
            command_descriptor[descriptor_index++] = current_descriptor_char;
            current_descriptor_char = command_str[++command_str_index];
        }

        int descriptor_is_valid = check_descriptor(command_descriptor);
        if (descriptor_is_valid == 1)
        {
            read_md_results.error_message = 2;
            read_md_results.command_number = command;
            fclose(metadata_fh);
            return read_md_results;
        }

        int descriptor_matches_component = check_descriptor_matches_component(component, command_descriptor);
        if (descriptor_matches_component == 1)
        {
            read_md_results.error_message = 3;
            read_md_results.command_number = command;
            fclose(metadata_fh);
            return read_md_results;
        }

        // extract the metadata command number
        reset_string(command_number);
        current_number_char = command_str[++command_str_index];
        number_index = 0;
        while (current_number_char != '\0')
        {
            command_number[number_index++] = current_number_char;
            current_number_char = command_str[++command_str_index];
        }

        // store all of the metadata command attributes into a struct
        command_array[command].component = component;

        strcpy(command_array[command].descriptor, command_descriptor);
        // deal with the component being memory, in which case there
        // are no cycles, thus cycles is set to 0
        if (component == 'M')
        {
            command_array[command].cycles = 0;
            sscanf(command_number, "%d", &command_memory);
            command_array[command].memory = command_memory;
        }
        // otherwise, memory is set to 0
        else
        {
            sscanf(command_number, "%d", &command_cycles);
            command_array[command].cycles = command_cycles;
            command_array[command].memory = 0;
        }

        set_time_to_run(&command_array[command], IO_cycle_time, CPU_cycle_time);
    }

    fclose(metadata_fh);

    *first_command = link_commands(command_array, commands_count);

    return read_md_results;
}

#endif