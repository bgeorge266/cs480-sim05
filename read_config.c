#ifndef READ_CONFIG_C
#define READ_CONFIG_C


#include "read_config.h"


int check_value_in_range(int value, int min, int max)
{
    if (value < min || value > max)
    {
        return 1;
    }

    return 0;
}


int check_scheduling(char* scheduling)
{
    int scheduling_is_correct = 0;
    char* scheduling_types[6] = {"NONE", "FCFS-N", "SJF-N", "SRTF-P", "FCFS-P", "RR-P"};
    int type = 0;

    for (; type < 6; type++)
    {
        if (strcmp(scheduling_types[type], scheduling) == 0)
        {
            scheduling_is_correct = 1;
        }
    }

    if (scheduling_is_correct == 0)
    {
        return 1;
    }

    return 0;
}


int check_log_to(char* log_to)
{
    int log_to_is_correct = 0;
    char* log_to_options[3] = {"Monitor", "File", "Both"};
    int option = 0;

    for (; option < 3; option++)
    {
        if (strcmp(log_to_options[option], log_to) == 0)
        {
            log_to_is_correct = 1;
        }
    }

    if (log_to_is_correct == 0)
    {
        return 1;
    }

    return 0;
}


struct error_data read_config(char* config_fp, struct config_data* config)
//struct config_data read_config(char* config_fp)
{
    struct error_data config_results;
    config_results.error_message = 0;
    config_results.line_number = 0;
    config_results.command_number = 0;

    // "boolean" variables to track whether values obtained from the configuration file are valid
    int file_ext_is_valid, config_fh_is_valid, version_is_valid, scheduling_is_valid, quantum_time_is_valid, mem_available_is_valid,
        cpu_cycle_time_is_valid, io_cycle_time_is_valid, log_to_is_valid;
    
    // "boolean" variables to track whether lines of data parsed from the configuration file match what is expected
    int start_line_is_valid, version_line_is_valid, mdf_fp_line_is_valid, scheduling_line_is_valid, quantum_time_line_is_valid,
        mem_available_line_is_valid, cpu_cycle_time_line_is_valid, io_cycle_time_line_is_valid, log_to_line_is_valid,
        log_fp_line_is_valid, end_line_is_valid;
    
    // variables used for tracking the parser's position in the configuration file
    FILE* config_fh;
    unsigned char next_char;
    int line_count = 1;
    int str_index;
    
    // struct config_data config;

    // these strings contain the expected beginning of the respective lines of the configuration file
    char expected_line_1[35] = "Start Simulator Configuration File";
    char expected_line_2[16] = "Version/Phase: ";
    char expected_line_3[12] = "File Path: ";
    char expected_line_4[22] = "CPU Scheduling Code: ";
    char expected_line_5[24] = "Quantum Time (cycles): ";
    char expected_line_6[24] = "Memory Available (KB): ";
    char expected_line_7[30] = "Processor Cycle Time (msec): ";
    char expected_line_8[24] = "I/O Cycle Time (msec): ";
    char expected_line_9[9] = "Log To: ";
    char expected_line_10[16] = "Log File Path: ";
    char expected_line_11[34] = "End Simulator Configuration File.";

    // these strings will contain the parsed beginning of each line in the configuration file, for comparison
    char received_line_1[35];
    char received_line_2[16];
    char received_line_3[12];
    char received_line_4[22];
    char received_line_5[24];
    char received_line_6[24];
    char received_line_7[30];
    char received_line_8[24];
    char received_line_9[9];
    char received_line_10[16];
    char received_line_11[34];

    // these strings will hold the important parsed data from the configuration file
    char version_str[4] = {"\0"};
    char mdf_filepath[60] = {"\0"};
    char CPU_scheduling[10] = {"\0"};
    char quantum_time_str[4] = {"\0"};
    char mem_available_str[4] = {"\0"};
    char processor_cycle_str[4] = {"\0"};
    char IO_cycle_str[4] = {"\0"};
    char log_to[8] = {"\0"};
    char lgf_filepath[60] = {"\0"};
    
    // any important parsed numeric data will be stored as such in these variables
    float version_float;
    int quantum_time_int;
    int mem_available_int;
    int processor_cycle_int;
    int IO_cycle_int;

    
    file_ext_is_valid = check_file_ext(config_fp, ".cnf");
    if (file_ext_is_valid == 1)
    {
        config_results.error_message = 1;
        return config_results;
    }

    config_fh = fopen(config_fp, "r");
    config_fh_is_valid = check_fh_is_valid(config_fh);
    if (config_fh_is_valid == 1)
    {
        config_results.error_message = 2;
        return config_results;
    }


    // This block of code simply skips the first line of the configuration file,
    // since it contains no information needed for the program.
    fgets(received_line_1, 35, config_fh);
    start_line_is_valid = check_line(expected_line_1, received_line_1);
    if (start_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;

    
    // This block of code is responsible for extracting the version number
    // from the configuration file and storing it as an int.
    fgets(received_line_2, 16, config_fh);
    version_line_is_valid = check_line(expected_line_2, received_line_2);
    if (version_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        version_str[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    sscanf(version_str, "%f", &version_float);
    config->version = (int)version_float;
    version_is_valid = check_value_in_range(config->version, 0, 10);
    if (version_is_valid == 1)
    {
        config_results.error_message = 4;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }


    // This block of code is responsible for extracting the metadata
    // filepath from the configuration file and storing it as a string.
    fgets(received_line_3, 12, config_fh);
    mdf_fp_line_is_valid = check_line(expected_line_3, received_line_3);
    if (mdf_fp_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        mdf_filepath[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    strcpy(config->mdf_filepath, mdf_filepath);

    
    // This block of code is responsible for extracting the CPU scheduling
    // code from the configuration file and storing it as a string.
    fgets(received_line_4, 22, config_fh);
    scheduling_line_is_valid = check_line(expected_line_4, received_line_4);
    if (scheduling_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        CPU_scheduling[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    strcpy(config->CPU_scheduling, CPU_scheduling);
    scheduling_is_valid = check_scheduling(config->CPU_scheduling);
    if (scheduling_is_valid == 1)
    {
        config_results.error_message = 5;
        fclose(config_fh);
        return config_results;
    }
    if (strcmp(config->CPU_scheduling, "NONE") == 0)
    {
        reset_string(config->CPU_scheduling);
        strcpy(config->CPU_scheduling, "FCFS-N");
    }

    
    // This block of code is responsible for extracting the quantum time
    // from the configuration file and storing it as an int.
    fgets(received_line_5, 24, config_fh);
    quantum_time_line_is_valid = check_line(expected_line_5, received_line_5);
    if (quantum_time_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        quantum_time_str[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    sscanf(quantum_time_str, "%d", &quantum_time_int);
    config->quantum_time = quantum_time_int;
    quantum_time_is_valid = check_value_in_range(config->quantum_time, 0, 100);
    if (quantum_time_is_valid)
    {
        config_results.error_message = 6;
        fclose(config_fh);
        return config_results;
    }

    
    // This block of code is responsible for extracting the available
    // memory from the configuration file and storing it as an int.
    fgets(received_line_6, 24, config_fh);
    mem_available_line_is_valid = check_line(expected_line_6, received_line_6);
    if (mem_available_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        mem_available_str[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    sscanf(mem_available_str, "%d", &mem_available_int);
    config->memory_available = mem_available_int;
    mem_available_is_valid = check_value_in_range(config->memory_available, 0, 1048576);
    if (mem_available_is_valid == 1)
    {
        config_results.error_message = 7;
        fclose(config_fh);
        return config_results;
    }


    // This block of code is responsible for extracting the processor
    // cycle time from the configuration file and storing it as an int.
    fgets(received_line_7, 30, config_fh);
    cpu_cycle_time_line_is_valid = check_line(expected_line_7, received_line_7);
    if (cpu_cycle_time_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        processor_cycle_str[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    sscanf(processor_cycle_str, "%d", &processor_cycle_int);
    config->CPU_cycle_time = processor_cycle_int;
    cpu_cycle_time_is_valid = check_value_in_range(config->CPU_cycle_time, 1, 1000);
    if (cpu_cycle_time_is_valid == 1)
    {
        config_results.error_message = 8;
        fclose(config_fh);
        return config_results;
    }


    // This block of code is responsible for extracting the I/O
    // cycle time from the configuration file and storing it as an int.
    fgets(received_line_8, 24, config_fh);
    io_cycle_time_line_is_valid = check_line(expected_line_8, received_line_8);
    if (io_cycle_time_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        IO_cycle_str[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    sscanf(IO_cycle_str, "%d", &IO_cycle_int);
    config->IO_cycle_time = IO_cycle_int;
    io_cycle_time_is_valid = check_value_in_range(config->IO_cycle_time, 1, 10000);
    if (io_cycle_time_is_valid == 1)
    {
        config_results.error_message = 9;
        fclose(config_fh);
        return config_results;
    }


    // This block of code is responsible for extracting the log option
    // from the configuration file and storing it as a string.
    fgets(received_line_9, 9, config_fh);
    log_to_line_is_valid = check_line(expected_line_9, received_line_9);
    if (log_to_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        log_to[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    strcpy(config->log_to, log_to);
    log_to_is_valid = check_log_to(config->log_to);
    if (log_to_is_valid == 1)
    {
        config_results.error_message = 10;
        fclose(config_fh);
        return config_results;
    }


    // This block of code is responsible for extracting the log
    // filepath from the configuration file and storing it as a string.
    fgets(received_line_10, 16, config_fh);
    log_fp_line_is_valid = check_line(expected_line_10, received_line_10);
    if (log_fp_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;
    next_char = fgetc(config_fh);
    str_index = 0;
    while (!isspace(next_char))
    {
        lgf_filepath[str_index++] = next_char;
        next_char = fgetc(config_fh);
    }
    skip_whitespace(config_fh);
    strcpy(config->lgf_filepath, lgf_filepath);
    if (strcmp(config->log_to, "File") != 0 &&
        strcmp(config->log_to, "Both") != 0)
    {
        strcpy(config->lgf_filepath, "NONE");
    }


    // This block of code simply skips the last line of the configuration file,
    // since it contains no information needed for the program.
    fgets(received_line_11, 34, config_fh);
    end_line_is_valid = check_line(expected_line_11, received_line_11);
    if (end_line_is_valid == 1)
    {
        config_results.error_message = 3;
        config_results.line_number = line_count;
        fclose(config_fh);
        return config_results;
    }
    skip_whitespace(config_fh);
    line_count++;

    fclose(config_fh);
    return config_results;
}


#endif