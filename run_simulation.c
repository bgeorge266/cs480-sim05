#ifndef RUN_SIMULATION_C
#define RUN_SIMULATION_C

#include "run_simulation.h"

// Moves a process into approprate queue from another
void move_process_to_queue(struct PCB_queue* from, struct PCB_queue* to, struct PCB_struct* process)
{
    struct PCB_struct* current_PCB;

    if (process == from->first_PCB)
    {
        from->first_PCB = from->first_PCB->next_PCB;
    }

    if (process->prev_PCB != NULL)
    {
        process->prev_PCB->next_PCB = process->next_PCB;
    }

    if (process->next_PCB != NULL)
    {
        process->next_PCB->prev_PCB = process->prev_PCB;
    }


    if (to->first_PCB == NULL)
    {
        to->first_PCB = process;
        process->prev_PCB = NULL;
        process->next_PCB = NULL;
    }

    else
    {
        current_PCB = to->first_PCB;
        while (current_PCB->next_PCB != NULL)
        {
            current_PCB = current_PCB->next_PCB;
        }
        current_PCB->next_PCB = process;

        process->prev_PCB = current_PCB;
        process->next_PCB = NULL;
    }


    from->size--;
    to->size++;

    if (from->size == 0)
    {
        from->first_PCB = NULL;
    }

    return;
}

// Implements FCFS scheduling and organizes queue apropriately
struct PCB_struct* select_by_FCFS(struct PCB_queue* ready)
{
    struct PCB_struct* current_PCB = ready->first_PCB;
    int lowest_process_num = current_PCB->process_num;
    struct PCB_struct* PCB_to_return = current_PCB;

    while (current_PCB != NULL)
    {
        if (current_PCB->process_num < lowest_process_num)
        {
            lowest_process_num = current_PCB->process_num;
            PCB_to_return = current_PCB;
        }

        current_PCB = current_PCB->next_PCB;
    }

    return PCB_to_return;
}

// Implements SJF scheduling and organizes queue apropriately
struct PCB_struct* select_by_shortest_job(struct PCB_queue* ready)
{
    struct PCB_struct* current_PCB = ready->first_PCB;
    int shortest_job_time = current_PCB->remaining_time_to_run;
    struct PCB_struct* PCB_to_return = current_PCB;

    while (current_PCB != NULL)
    {
        if (current_PCB->remaining_time_to_run < shortest_job_time)
        {
            shortest_job_time = current_PCB->remaining_time_to_run;
            PCB_to_return = current_PCB;
        }

        current_PCB = current_PCB->next_PCB;
    }

    return PCB_to_return;
}

// Implements RR scheduling and organizes queue apropriately
struct PCB_struct* select_by_RR(struct PCB_queue* ready)
{
    return ready->first_PCB;
}

 // Checks which scheduling code is specified in the config document
struct PCB_struct* select_by_scheduling(struct PCB_queue* ready, char* scheduling_code)
{
    if ((strcmp(scheduling_code, "FCFS-N") == 0) || (strcmp(scheduling_code, "FCFS-P") == 0))
    {
        return select_by_FCFS(ready);
    }

    else if ((strcmp(scheduling_code, "SJF-N") == 0) || (strcmp(scheduling_code, "SRTF-P") == 0))
    {
        return select_by_shortest_job(ready);
    }

    else
    {
        return select_by_RR(ready);
    }
}

// runs non pre-emptive schedules
struct error_data run_simulation_N(struct md_command first_command, int commands_count, struct config_data *config)
{
    int write_to_file = 0, write_to_monitor = 0;
    int log_file_ext_is_valid;
    FILE* log_fh;
    char output_buffer[100000];
    char line_buffer[100];

    char log_to[8];
    char log_filepath[60];
    char scheduling_code[7];
    struct error_data run_simulation_results;

    char component, descriptor[11];
    float time_to_wait;
    int current_process_num, processes_count;
    struct md_command *process_starters, current_command;

    struct timeval before, after;
    char* time_string;
    long time_difference = 0;

    struct PCB_struct first_PCB;
    struct PCB_struct* current_PCB;
    int commands_executed, commands_to_execute;
    pthread_t wait_thread;

    struct PCB_queue new;
    struct PCB_queue ready;
    struct PCB_queue running;
    struct PCB_queue exit;


    strcpy(log_to, config->log_to);

    // sets flags depending on log to
    if (strcmp(log_to, "File") == 0)
    {
        write_to_file = 1;
    }

    else if (strcmp(log_to, "Monitor") == 0)
    {
        write_to_monitor = 1;
    }

    else if (strcmp(log_to, "Both") == 0)
    {
        write_to_file = 1;
        write_to_monitor = 1;
    }


    strcpy(log_filepath, config->lgf_filepath);

    // checks for valid log file path
    if (write_to_file == 1)
    {
        log_file_ext_is_valid = check_file_ext(log_filepath, ".lgf");
        if (log_file_ext_is_valid == 1)
        {
            run_simulation_results.error_message = 1;
            return run_simulation_results;
        }

        log_fh = fopen(log_filepath, "w");
    }

    run_simulation_results.error_message = 0;

    strcpy(scheduling_code, config->CPU_scheduling);

    // gets initial time
    get_time("start", NULL, &before, NULL);
    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s, System start\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
    }

    // setting sizes
    new.size = 0;
    ready.size = 0;
    running.size = 0;
    exit.size = 0;


    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s OS: Begin PCB Creation\n", time_string);
    strcat(output_buffer, line_buffer);

    // if moniitor flag set, print to monitor
    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
    }

    // get process count
    processes_count = count_processes(first_command, commands_count);

    process_starters = malloc(processes_count * sizeof(struct md_command));
    find_process_starters(first_command, commands_count, process_starters);

    first_PCB = create_PCBs(processes_count, process_starters, commands_count);

    new.size = processes_count;
    new.first_PCB = &first_PCB;


    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s, OS: All processes initialized in New state\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
    }

    // loop to move processes into queue
    while (new.size > 0)
    {
        move_process_to_queue(&new, &ready, new.first_PCB);
    }


    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    // processes now in ready
    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s, OS: All processes now set in Ready state\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
    }

    // loop for each rocess in queue
    while (exit.size < processes_count)
    {
        current_PCB = select_by_scheduling(&ready, scheduling_code);

        // get info from current pcb to store in temp variables
        current_process_num = current_PCB->process_num;
        commands_executed = current_PCB->commands_executed;
        commands_to_execute = current_PCB->total_commands_to_execute;


        time_string = get_time("stop", &time_difference, &before, &after);
        get_time("start", NULL, &before, NULL);

        reset_string(line_buffer);
        sprintf(line_buffer, "Time: %s, OS: %s selects Process %d\n", time_string, scheduling_code, current_process_num);
        strcat(output_buffer, line_buffer);

        if (write_to_monitor == 1)
        {
            fprintf(stdout, "%s", line_buffer);
        }


        for (; commands_executed < commands_to_execute; commands_executed++)
        {
            current_command = current_PCB->process_commands[commands_executed];
            component = current_command.component;
            reset_string(descriptor);
            strcpy(descriptor, current_command.descriptor);
            time_to_wait = current_command.time_to_run * 500;

            // depending on action recieved from meta data
            switch(component)
            {
                case 'A':
                    if (strcmp(descriptor, "start") == 0)
                    {
                        move_process_to_queue(&ready, &running, current_PCB);

                        time_string = get_time("stop", &time_difference, &before, &after);
                        get_time("start", NULL, &before, NULL);

                        reset_string(line_buffer);
                        sprintf(line_buffer, "Time: %s, OS: Process %d set in Running state\n", time_string, current_process_num);
                        strcat(output_buffer, line_buffer);

                        if (write_to_monitor == 1)
                        {
                            fprintf(stdout, "%s", line_buffer);
                        }
                    }

                    else
                    {
                        move_process_to_queue(&running, &exit, current_PCB);

                        time_string = get_time("stop", &time_difference, &before, &after);
                        get_time("start", NULL, &before, NULL);

                        reset_string(line_buffer);
                        sprintf(line_buffer, "Time: %s, OS: Process %d set in Exit state\n", time_string, current_process_num);
                        strcat(output_buffer, line_buffer);

                        if (write_to_monitor == 1)
                        {
                            fprintf(stdout, "%s", line_buffer);
                        }
                    }

                    break;


                case 'P':
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, Run operation start\n", time_string, current_process_num);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }


                    wait(time_to_wait);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, Run operation end\n", time_string, current_process_num);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }

                    break;


                case 'M':
                    wait(time_to_wait);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, Memory management %s action\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }

                    break;


                case 'I':
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, %s input action start\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }


                    pthread_create(&wait_thread, NULL, threaded_wait_N, &time_to_wait);
                    pthread_join(wait_thread, NULL);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, %s input action end\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }

                    break;


                case 'O':
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, %s output action start\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }

                    // create pthread for waiting
                    pthread_create(&wait_thread, NULL, threaded_wait_N, &time_to_wait);
                    pthread_join(wait_thread, NULL);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, %s output action end\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }

                    break;
            }
        }
    }


    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    // resets buffer for storing new line to print
    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s, System stop\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
        fprintf(stdout, "\nEnd Simulation\n");
        fprintf(stdout, "==============\n");
    }

    // outputs to log prior to closing
    if (write_to_file == 1)
    {
        fprintf(log_fh, "%s", output_buffer);
        fclose(log_fh);
    }


    return run_simulation_results;
}

// similar to run_sim but with preemption and interrupts in mind
struct error_data run_simulation_P(struct md_command first_command, int commands_count, struct config_data *config)
{
    int write_to_file = 0, write_to_monitor = 0;
    int log_file_ext_is_valid;
    FILE* log_fh;
    char output_buffer[100000];
    char line_buffer[100];

    char log_to[8];
    char log_filepath[60];
    char scheduling_code[7];
    int quantum_time;
    int CPU_cycle_time;
    struct error_data run_simulation_results;

    char component, descriptor[11];
    float time_to_wait;
    int current_process_num, processes_count;
    struct md_command *process_starters, current_command;

    struct timeval before, after;
    char* time_string;
    long time_difference = 0;

    struct PCB_struct first_PCB;
    struct PCB_struct* current_PCB;
    struct PCB_struct* interrupt_PCB;
    int interrupt_process_num;
    int commands_executed;
    int cycles_elapsed;
    int process_is_finished;
    int cycles_to_run;

    pthread_t wait_thread;
    struct wait_P_data_struct wait_P_data;

    struct PCB_queue new;
    struct PCB_queue ready;
    struct PCB_queue running;
    struct PCB_queue blocked;
    struct PCB_queue interrupts;
    struct PCB_queue exit;


    strcpy(log_to, config->log_to);

    if (strcmp(log_to, "File") == 0)
    {
        write_to_file = 1;
    }

    else if (strcmp(log_to, "Monitor") == 0)
    {
        write_to_monitor = 1;
    }

    else if (strcmp(log_to, "Both") == 0)
    {
        write_to_file = 1;
        write_to_monitor = 1;
    }


    strcpy(log_filepath, config->lgf_filepath);

    if (write_to_file == 1)
    {
        log_file_ext_is_valid = check_file_ext(log_filepath, ".lgf");
        if (log_file_ext_is_valid == 1)
        {
            run_simulation_results.error_message = 1;
            return run_simulation_results;
        }

        log_fh = fopen(log_filepath, "w");
    }

    CPU_cycle_time = config->CPU_cycle_time;

    run_simulation_results.error_message = 0;

    strcpy(scheduling_code, config->CPU_scheduling);


    get_time("start", NULL, &before, NULL);
    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s, System start\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
    }


    new.size = 0;
    ready.size = 0;
    running.size = 0;
    blocked.size = 0;
    interrupts.size = 0;
    exit.size = 0;


    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s OS: Begin PCB Creation\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
    }


    processes_count = count_processes(first_command, commands_count);

    process_starters = malloc(processes_count * sizeof(struct md_command));
    find_process_starters(first_command, commands_count, process_starters);

    first_PCB = create_PCBs(processes_count, process_starters, commands_count);

    new.size = processes_count;
    new.first_PCB = &first_PCB;


    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s, OS: All processes initialized in New state\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
    }


    while (new.size > 0)
    {
        move_process_to_queue(&new, &ready, new.first_PCB);
    }


    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);

    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s, OS: All processes now set in Ready state\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
    }


    int i_count = 0;


    while (exit.size < processes_count)
    {
        current_PCB = select_by_scheduling(&ready, scheduling_code);
        current_process_num = current_PCB->process_num;


        time_string = get_time("stop", &time_difference, &before, &after);
        get_time("start", NULL, &before, NULL);

        reset_string(line_buffer);
        sprintf(line_buffer, "Time: %s, OS: %s selects Process %d\n", time_string, scheduling_code, current_process_num);
        strcat(output_buffer, line_buffer);

        if (write_to_monitor == 1)
        {
            fprintf(stdout, "%s", line_buffer);
        }


        move_process_to_queue(&ready, &running, current_PCB);

        time_string = get_time("stop", &time_difference, &before, &after);
        get_time("start", NULL, &before, NULL);

        reset_string(line_buffer);
        sprintf(line_buffer, "Time: %s, OS: Process %d set in Running state\n", time_string, current_process_num);
        strcat(output_buffer, line_buffer);

        if (write_to_monitor == 1)
        {
            fprintf(stdout, "%s", line_buffer);
        }

        // keeps track of which commands have been executed
        commands_executed = current_PCB->commands_executed;

        current_command = current_PCB->process_commands[commands_executed];
        component = current_command.component;
        reset_string(descriptor);
        strcpy(descriptor, current_command.descriptor);
        time_to_wait = current_command.time_to_run * 500;


        quantum_time = config->quantum_time;
        cycles_elapsed = 0;
        process_is_finished = 0;


        if (current_PCB->remaining_cycles_in_action == 0)
        {
            cycles_to_run = current_command.cycles;
        }

        else
        {
            cycles_to_run = current_PCB->remaining_cycles_in_action;
        }

        // while loop to handle interupts and the states that occur due to them
        while ((cycles_elapsed < quantum_time) && (interrupts.size == 0) && (!process_is_finished))
        {
            if ((cycles_elapsed == cycles_to_run) && (component != 'A'))
            {
                commands_executed++;
                current_PCB->commands_executed++;

                quantum_time = quantum_time - cycles_elapsed;
                cycles_elapsed = 0;

                current_command = current_PCB->process_commands[commands_executed];
                component = current_command.component;
                reset_string(descriptor);
                strcpy(descriptor, current_command.descriptor);
                time_to_wait = current_command.time_to_run * 500;
                cycles_to_run = current_command.cycles;
            }


            switch(component)
            {
                case 'A':
                    if (strcmp(descriptor, "start") == 0)
                    {
                        commands_executed++;
                        current_PCB->commands_executed++;

                        current_command = current_PCB->process_commands[commands_executed];
                        component = current_command.component;
                        reset_string(descriptor);
                        strcpy(descriptor, current_command.descriptor);
                        time_to_wait = current_command.time_to_run * 500;
                        cycles_to_run = current_command.cycles;
                    }

                    else
                    {
                        move_process_to_queue(&running, &exit, current_PCB);

                        time_string = get_time("stop", &time_difference, &before, &after);
                        get_time("start", NULL, &before, NULL);

                        reset_string(line_buffer);
                        sprintf(line_buffer, "Time: %s, OS: Process %d set in Exit state\n", time_string, current_process_num);
                        strcat(output_buffer, line_buffer);

                        if (write_to_monitor == 1)
                        {
                            fprintf(stdout, "%s", line_buffer);
                        }

                        process_is_finished = 1;
                    }

                    break;


                case 'P':
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, Run operation start\n", time_string, current_process_num);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }


                    while ((cycles_elapsed < quantum_time) && (cycles_elapsed < cycles_to_run) && (interrupts.size == 0))
                    {
                        wait(CPU_cycle_time * 500);
                        cycles_elapsed++;
                    }


                    if (interrupts.size > 0)
                    {
                        current_PCB->remaining_cycles_in_action = cycles_to_run - cycles_elapsed;
                        move_process_to_queue(&running, &ready, current_PCB);

                        time_string = get_time("stop", &time_difference, &before, &after);
                        get_time("start", NULL, &before, NULL);

                        reset_string(line_buffer);
                        sprintf(line_buffer, "Time: %s, OS: Process %d set to Ready state\n", time_string, current_process_num);
                        strcat(output_buffer, line_buffer);

                        if (write_to_monitor == 1)
                        {
                            fprintf(stdout, "%s", line_buffer);
                        }
                    }

                    else if (cycles_elapsed ==  quantum_time)
                    {
                        current_PCB->remaining_cycles_in_action = cycles_to_run - cycles_elapsed;
                        move_process_to_queue(&running, &ready, current_PCB);

                        time_string = get_time("stop", &time_difference, &before, &after);
                        get_time("start", NULL, &before, NULL);

                        reset_string(line_buffer);
                        sprintf(line_buffer, "Time: %s, OS: Process %d set to Ready state\n", time_string, current_process_num);
                        strcat(output_buffer, line_buffer);

                        if (write_to_monitor == 1)
                        {
                            fprintf(stdout, "%s", line_buffer);
                        }
                    }

                    else
                    {
                        time_string = get_time("stop", &time_difference, &before, &after);
                        get_time("start", NULL, &before, NULL);

                        reset_string(line_buffer);
                        sprintf(line_buffer, "Time: %s, Process %d, Run operation end\n", time_string, current_process_num);
                        strcat(output_buffer, line_buffer);

                        if (write_to_monitor == 1)
                        {
                            fprintf(stdout, "%s", line_buffer);
                        }
                    }

                    break;


                case 'M':
                    wait(time_to_wait);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, Memory management %s action\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }

                    break;


                case 'I':

                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, %s input action start\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }


                    move_process_to_queue(&running, &blocked, current_PCB);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, OS: Process %d set in Blocked state\n", time_string, current_process_num);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }

                    // if no processes are left in ready or running, display a message to indicate that the system is idling
                    if (ready.size == 0 && running.size == 0)
                    {
                      time_string = get_time("stop", &time_difference, &before, &after);
                      get_time("start", NULL, &before, NULL);

                      reset_string(line_buffer);
                      sprintf(line_buffer, "Time: %s, all remaining processes in Blocked state -- system is idling.\n", time_string);
                      strcat(output_buffer, line_buffer);

                      if (write_to_monitor == 1)
                      {
                        fprintf(stdout, "%s", line_buffer);
                      }
                    }


                    wait_P_data.time_to_wait_us = time_to_wait;
                    wait_P_data.blocked = &blocked;
                    wait_P_data.interrupts = &interrupts;
                    wait_P_data.process = current_PCB;


                    pthread_create(&wait_thread, NULL, threaded_wait_P, &wait_P_data);
                    pthread_join(wait_thread, NULL);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, %s input action end\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }


                    commands_executed++;
                    current_PCB->commands_executed++;
                    current_command = current_PCB->process_commands[commands_executed];

                    i_count++;

                    break;


                case 'O':
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, %s output action start\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }


                    move_process_to_queue(&running, &blocked, current_PCB);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, OS: Process %d set in Blocked state\n", time_string, current_process_num);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }

                    // if no processes are left in ready or running, display a message to indicate that the system is idling
                    if (ready.size == 0 && running.size == 0)
                    {
                      time_string = get_time("stop", &time_difference, &before, &after);
                      get_time("start", NULL, &before, NULL);

                      reset_string(line_buffer);
                      sprintf(line_buffer, "Time: %s, all remaining processes in Blocked state -- system is idling.\n", time_string);
                      strcat(output_buffer, line_buffer);

                      if (write_to_monitor == 1)
                      {
                        fprintf(stdout, "%s", line_buffer);
                      }
                    }

                    wait_P_data.time_to_wait_us = time_to_wait;
                    wait_P_data.blocked = &blocked;
                    wait_P_data.interrupts = &interrupts;
                    wait_P_data.process = current_PCB;


                    pthread_create(&wait_thread, NULL, threaded_wait_P, &wait_P_data);
                    pthread_join(wait_thread, NULL);
                    time_string = get_time("stop", &time_difference, &before, &after);
                    get_time("start", NULL, &before, NULL);

                    reset_string(line_buffer);
                    sprintf(line_buffer, "Time: %s, Process %d, %s output action end\n", time_string, current_process_num, descriptor);
                    strcat(output_buffer, line_buffer);

                    if (write_to_monitor == 1)
                    {
                        fprintf(stdout, "%s", line_buffer);
                    }


                    commands_executed++;
                    current_PCB->commands_executed++;
                    current_command = current_PCB->process_commands[commands_executed];

                    break;
            }
        }

        // interupt loop that continues until size of array is 0
        while (interrupts.size > 0)
        {
            interrupt_PCB = interrupts.first_PCB;
            interrupt_process_num = interrupt_PCB->process_num;

            move_process_to_queue(&interrupts, &ready, interrupt_PCB);
            time_string = get_time("stop", &time_difference, &before, &after);
            get_time("start", NULL, &before, NULL);

            reset_string(line_buffer);
            sprintf(line_buffer, "Time: %s, OS: Process %d set in Ready state\n", time_string, interrupt_process_num);
            strcat(output_buffer, line_buffer);

            if (write_to_monitor == 1)
            {
                fprintf(stdout, "%s", line_buffer);
            }
        }
    }


    time_string = get_time("stop", &time_difference, &before, &after);
    get_time("start", NULL, &before, NULL);


    reset_string(line_buffer);
    sprintf(line_buffer, "Time: %s, System stop\n", time_string);
    strcat(output_buffer, line_buffer);

    if (write_to_monitor == 1)
    {
        fprintf(stdout, "%s", line_buffer);
        fprintf(stdout, "\nEnd Simulation\n");
        fprintf(stdout, "==============\n");
    }

    // outputs to log prior to closing
    if (write_to_file == 1)
    {
        fprintf(log_fh, "%s", output_buffer);
        fclose(log_fh);
    }


    return run_simulation_results;
}


#endif