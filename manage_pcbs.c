#ifndef MANAGE_PCBS_C
#define MANAGE_PCBS_C


#include "manage_pcbs.h"


int check_process_start(struct md_command command)
{
    if (command.component == 'A')
    {
        if (strcmp(command.descriptor, "start") == 0)
        {
            return 1;
        }
    }

    return 0;
}


int count_processes(struct md_command current_command, int commands_count)
{
    int current_command_num = 0, processes_count = 0;

    for (; current_command_num < commands_count; current_command_num++)
    {
        if (check_process_start(current_command) == 1)
        {
            processes_count++;
        }

        if (current_command.next != NULL)
        {
            current_command = *current_command.next;
        }
    }

    return processes_count;
}


void find_process_starters(struct md_command current_command, int commands_count, struct md_command* process_starters)
{
    int commands_counted = 0, processes_counted = 0;
    for (; commands_counted < commands_count; commands_counted++)
    {
        if (check_process_start(current_command) == 1)
        {
            process_starters[processes_counted++] = current_command;
        }

        if (current_command.next != NULL)
        {
            current_command = *current_command.next;
        }
    }
}


int count_commands_in_process(struct md_command current_command)
{
    int commands_counted = 2;
    while (current_command.next->component != 'A')
    {
        commands_counted++;
        current_command = *current_command.next;
    }

    return commands_counted;
}


void fill_process_array(struct md_command first_command, int commands_in_process, struct md_command* commands_array)
{
    struct md_command current_command = first_command;
    int commands_stored = 0;
    
    for (; commands_stored < commands_in_process; commands_stored++)
    {
        commands_array[commands_stored] = current_command;
        current_command = *current_command.next;
    }
}


void set_process_runtime(struct PCB_struct* current_PCB)
{
    // take in a pointer to a PCB
    // grab its commands
    struct md_command* current_command = current_PCB->process_commands;
    float total_runtime = 0;
    // tally up their runtime
    while (current_command != NULL && !(current_command->component == 'A' && strcmp(current_command->descriptor, "end") == 0))
    {
        total_runtime = total_runtime + current_command->time_to_run;
        current_command = current_command->next;
    }
    // save the sum into the PCB
    current_PCB->remaining_time_to_run = total_runtime;
}


struct PCB_struct create_PCBs(int processes_count, struct md_command* process_starters, int commands_count)
{
    int current_process_num = 0, commands_in_process;
    struct md_command first_process_command;
    struct PCB_struct *current_PCB;
    current_PCB = malloc(sizeof(struct PCB_struct));

    for (; current_process_num < processes_count; current_process_num++)
    {
        current_PCB->process_num = current_process_num;
        first_process_command = process_starters[current_process_num];

        commands_in_process = count_commands_in_process(first_process_command);
        current_PCB->total_commands_to_execute = commands_in_process;

        current_PCB->commands_executed = 0;
        current_PCB->process_commands = malloc(commands_count * sizeof(struct md_command));
        fill_process_array(first_process_command, commands_in_process, current_PCB->process_commands);
        
        set_process_runtime(current_PCB);
        current_PCB->remaining_cycles_in_action = 0;

        if (current_process_num != processes_count - 1)
        {
            current_PCB->next_PCB = malloc(sizeof(struct PCB_struct));
            current_PCB->next_PCB->prev_PCB = current_PCB;
            current_PCB = current_PCB->next_PCB;
        }
    }

    current_process_num--;

    for (; current_process_num > 0; current_process_num--)
    {
    	if (current_PCB->prev_PCB != NULL)
        {
            current_PCB = current_PCB->prev_PCB;
        }
    }

    current_PCB->prev_PCB = NULL;

    return *current_PCB;
}


#endif