#ifndef RUN_SIMULATION_H
#define RUN_SIMULATION_H


#include <pthread.h>
#include <string.h>
#include <sys/time.h>

#include "file_helpers.h"
#include "read_config.h"
#include "read_metadata.h"
#include "stopwatch.h"
#include "manage_pcbs.h"


struct PCB_queue
{
	int size;
	struct PCB_struct* first_PCB;
};


void move_process_to_queue(struct PCB_queue* from, struct PCB_queue* to, struct PCB_struct* process);

struct PCB_struct* select_by_FCFS(struct PCB_queue* ready);

struct PCB_struct* select_by_shortest_job(struct PCB_queue* ready);

struct PCB_struct* select_by_RR(struct PCB_queue* ready);

struct PCB_struct* select_by_scheduling(struct PCB_queue* ready, char* scheduling_code);

struct error_data run_simulation_N(struct md_command first_command, int commands_count, struct config_data *config);

struct error_data run_simulation_P(struct md_command first_command, int commands_count, struct config_data *config);


#endif